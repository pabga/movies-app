package com.example.moviesapp.ui.moviedetails

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.databinding.FragmentMovieDetailBinding


class MovieDetailFragment : Fragment(R.layout.fragment_movie_detail) {

    private val args by navArgs<MovieDetailFragmentArgs>()

    private lateinit var binding: FragmentMovieDetailBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentMovieDetailBinding.bind(view)
        Glide.with(requireContext()).load("https://image.tmdb.org/t/p/w500/${args.posterImageUrl}").centerCrop().into(binding.imgMovie)
        Glide.with(requireContext()).load("https://image.tmdb.org/t/p/w500/${args.backgroundImageUrl}").centerCrop().into(binding.imgBackground)

        binding.txtDescription.text = args.overview
        binding.txtTittle.text = args.tittle
        binding.txtLanguage.text = "Language: ${args.originalLanguage}"
        binding.txtRating.text = "${args.voteAverage} (${args.voteCount} Reviews) "
        binding.txtReleased.text = "Released ${args.releasedate}"






    }

}