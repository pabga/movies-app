# Movies App
Movies App is a simple mobile application where you can see the top , popular, and upcoming movies.

***
## Description
This app has been developed following the MVVM architecture

## Tools

- Room
- Glide
- Retrofit

## Test and Deploy

Use the built-in continuous integration in GitLab.

![anigif.gif](anigif.gif)


